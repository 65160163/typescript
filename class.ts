class Person {
    
  
    public constructor(private readonly name: string) {
     
    }
  
    public getName(): string {
      return this.name;
    }
  }
        
  const person = new Person("Jane");
  
  console.log(person.getName()); 
  
  console.log(person);

  interface Shape {
    getArea: () => number;
  }
  
  class Rectangle1 implements Shape {
    public constructor(protected readonly width: number, protected readonly height: number) {}

    public getArea(): number {
      return this.width * this.height;
    }
  }
  
  const myRect = new Rectangle1(50,10);
  
  console.log(myRect);
  console.log(myRect.getArea());

  class Square extends Rectangle1 {
    public constructor(width: number) {
      super(width, width);
      
    }
    public override toString(): string {
        return `Square[width=${this.width}]`;
      }
  
  }
  const mySq = new Square(10);
  console.log(mySq);
console.log(mySq.getArea());


console.log(mySq.toString());

abstract class Polygon {
    public abstract getArea(): number;
  
    public toString(): string {
      return `Polygon[area=${this.getArea()}]`;
    }
  }
  
  class Rectangle2 extends Polygon {
    public constructor(protected readonly width: number, protected readonly height: number) {
      super();
    }
  
    public getArea(): number {
      return this.width * this.height;
    }
  }
  
  const Rect = new Rectangle2(10,20);
  
  console.log(Rect.getArea());